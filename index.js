const crypto = require('crypto');
const got = require('got');
const fs = require('fs');
const algorithm = 'aes-256-ctr';

let token = 'r3SQ3sWU59Sqm2Gw6Gcf6aIYZoY2p2mm'
let address = 'XuoNuSGP4QAUFUs6w9DvhCAmA2mD4bPVmb'

let key_part = '658f95c1890dfd176a';
let iv = "c1890dd176658f95c1890dd176658f95"

const trading_signal_generator_id_MAP = {
    '01': 'ETH_LTC_25',
    '02': 'ETH_LTC_20',
    '03': 'ETH_BCH_96',
    '04': 'ETH_LTC_10',
    '05': 'ETH_ETH_98',
    '06': 'BTC_BCH_9',
    '07': 'BTC_BTC_17',
    '08': 'BTC_LTC_55',
    '09': 'BTC_LTC_38',
    '10': 'BCH_LTC_38',
    '11': 'BCH_BCH_96',
    '12': 'BCH_BCH_39',
    '13': 'BCH_BCH_125',
    '14': 'BCH_BCH_159',
    '15': 'LTC_LTC_38',
    '16': 'LTC_LTC_55',
    '17': 'LTC_BCH_92',
    '18': 'LTC_LTC_50'
}

function hex_to_ascii(str1) {
	var hex  = str1.toString();
	var str = '';
	for (var n = 0; n < hex.length; n += 2) {
		str += String.fromCharCode(parseInt(hex.substr(n, 2), 16));
	}
	return str;
}

async function getTransactionsForPage(pageNum) {
    return new Promise(async function(resolve, reject) {
        let url = `https://api.chainrider.io/v1/dash/main/txs?address=${address}&token=${token}&pageNum=${pageNum}`;
        got(url).then(response => {
            resolve(response.body);
        }).catch(error => {
            console.log(error);
            return reject(error);
        })
    })
}

function decrypt(hash_content, secretKey, iv) {
    hash = {
        iv,
        content: hash_content
    }
    const decipher = crypto.createDecipheriv(algorithm, secretKey, Buffer.from(hash.iv, 'hex'));
    const decrpyted = Buffer.concat([decipher.update(Buffer.from(hash.content, 'hex')), decipher.final()]);
    return decrpyted.toString();
};

function saveCSVFile(rows) {
    const filename = 'data.csv';
    const header = ["signal,signal_generator_id,timestamp"];
    const data = header.concat(rows).join("\n");
    fs.writeFile(filename, data, err => {
        if (err) {
          console.log('Error writing to csv file', err);
        } else {
          console.log(`Saved as ${filename}`);
        }
    });
}

async function main() {
    let allTransactions = [];
    let pagesTotal = 1;

    for(let page = 0; page < pagesTotal; page++) {
        let res = await getTransactionsForPage(page);
        res = JSON.parse(res);
        pagesTotal = res.pagesTotal;
        let tmp = res.txs;
        allTransactions = [...allTransactions, ...tmp];
        process.stdout.write('Fetching data... (' + Math.round(((page + 1)*100 /pagesTotal ) * 100) / 100 + '%) \r');
    }

    let timestamps = allTransactions.map(x => new Date(x.time * 1000))
    let allSignals = allTransactions.slice(0, allTransactions.length - 1).map(x => hex_to_ascii(x.vout[0].scriptPubKey.hex).split('"')[1])

    
    let decripted = []
    let timeForDecripted = []
    for(let i = 0; i < allSignals.length - 25; i++) {
        let key = allSignals[i].slice(6);
        if(key) {
            const text = decrypt(allSignals[i+25].slice(0, 6), key + key_part, iv);
            decripted.push(text);
            timeForDecripted.push(timestamps[i+25])
        } else {
            continue;
        }
    }

    let rows = []
    for(let i = 0; i < decripted.length; i++) {
        let signal = decripted[i].slice(0, 1) == 1 ? 'Buy' : 'Sell';
        let generator_id = trading_signal_generator_id_MAP[decripted[i].slice(1)];
        let time = timeForDecripted[i]
        rows.push(`${signal},${generator_id},${time.getTime()}`);
        console.log(signal + '\t' + generator_id + "\t["+time.toISOString().replace('T', ' ').slice(0, 19)+"]")
    }

    console.log(`\nNumber of decoded items ${decripted.length}\n`);
    
    saveCSVFile(rows);
}

main();
