# VLADAM - Generated trading signals decryptor
&nbsp; 
##### This NodeJS script is used for fast and automatic decrypting of all encrypted VLADAM trading signals from DASH public blockchain.

##### All encrypted trading signals generated on the VLADAM platform you can see on this [link](https://chainz.cryptoid.info/dash/address.dws?XuoNuSGP4QAUFUs6w9DvhCAmA2mD4bPVmb.htm).
&nbsp; 

##### By running this script you will get all decrypted trading signals in your console and in CSV file as well.
##### Every decrypted trading signal has information about action (buy/sell), name of the trading signal generator which generated it, and time of transaction on the blockchain (which is at most 2 minutes later than when the signal was generated)
&nbsp;

### This is all you need:
- Node (version 10.0+)
- NPM (version 6.0+)
&nbsp;
### Follow these steps:
##### 1) Clone this repository 

##### 2) Run command to install dependencies:

```
npm i
```
##### 3) When it is done you can finally run the command to start the script


```
npm start
```

